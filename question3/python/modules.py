from threading import local


def catch_data(number_of_oxen):
    array_with_datas = []
    for i in range (1, number_of_oxen+1):
        data = {}
        data['name'] = input('Digite o nome do boi: ')
        data['mass'] = float(input(f'Digite a massa do {data["name"]}: '))
        array_with_datas.append(data)
    return array_with_datas


def verify_fat(array_of_oxen):
    fat_ref = 0
    for j in range(0, len(array_of_oxen)):
        if array_of_oxen[j]['mass'] >= fat_ref:
            fat_ref = array_of_oxen[j]['mass']
            local_of_array_fat = j
    return array_of_oxen[local_of_array_fat], local_of_array_fat+1

def verify_skinny(array_of_oxen):
    skinny_ref = 10**10
    for k in range(0, len(array_of_oxen)):
        if array_of_oxen[k]['mass'] <= skinny_ref:
            skinny_ref = array_of_oxen[k]['mass']
            local_of_array_skinny = k
    return array_of_oxen[local_of_array_skinny], local_of_array_skinny+1
