import modules


number_of_oxen = int(input('Quantos bois ambrósio tem? ')) # Reads how many oxen ambrósio has
array_of_oxen = modules.catch_data(number_of_oxen) #Create a array of dictionaries, with the name and mass of each ox inside
fat, id_of_fat = modules.verify_fat(array_of_oxen) #Verify the most fat ox that ambrósio has
skinny, id_of_skinny = modules.verify_skinny(array_of_oxen) #Verify the most skinny ox that ambrósio has
print(f'Gordo: id: i{id_of_fat} | peso: {fat["mass"]:.2f} kg | nome: {fat["name"]}')
print(f'Magro: id: i{id_of_skinny} | peso: {skinny["mass"]:.2f} kg | nome: {skinny["name"]}')