import {catchData, verifyFat, verifySkinny} from './modules.mjs'

import promptSync from 'prompt-sync'

const prompt = promptSync()

const numberOfOxen = prompt("Quantos bois o ambrósio tem? ")
let arrayOfOxen = catchData(numberOfOxen)
const fat = verifyFat(arrayOfOxen)
const skinny = verifySkinny(arrayOfOxen)
console.log(`Gordo: id: i${fat['id']} | peso: ${fat['mass']} kg | nome: ${fat['name']}`)
console.log(`Magro: id: i${skinny['id']} | peso: ${skinny['mass']} kg | nome: ${skinny['name']}`)
