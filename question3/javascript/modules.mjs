import promptSync from 'prompt-sync'

const prompt = promptSync()

export let catchData = (numberOfOxen)=>{
    let arrayOfOxen = []
    for (let i = 0; i<numberOfOxen; i++){
        let ox = {}
        ox['name'] = prompt("Digite o nome do boi: ")
        ox['mass'] = Number(prompt(`Digite o a massa do ${ox['name']}: `))
        ox['id'] = i+1
        arrayOfOxen.push(ox)
    
    }
    return arrayOfOxen
}

export let verifyFat = (arrayOfOxen)=>{
    let fat = 0
    let fatToKeep;
    for (let j = 0; j<arrayOfOxen.length; j++){
        if (arrayOfOxen[j]['mass'] >= fat){
            fat = arrayOfOxen[j]['mass']
            fatToKeep = arrayOfOxen[j]
        }
    }
    return fatToKeep
}

export let verifySkinny = (arrayOfOxen)=>{
    let skinny = 10**10
    let skinnyToKeep;
    for (let k = 0; k<arrayOfOxen.length; k++){
        if (arrayOfOxen[k]['mass'] < skinny){
            skinny = arrayOfOxen[k]['mass']
            skinnyToKeep = arrayOfOxen[k]
        }
    }
    return skinnyToKeep
}