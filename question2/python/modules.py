from sys import modules

def read_20_num(): #function that read 20 integers
    arraylist = []
    i=0
    readInt = int(input('Digite o x0: '))
    arraylist.append(readInt)
    while i < 19:
        if readInt == int(-1):
            break
        else:
            i += 1
            readInt = int(input(f'Digite o x{i}: '))
            arraylist.append(readInt)
    return arraylist


def analyze_num(arrayList, num): #analyze how many times the select number appears in the array
    sum = 0
    for c in arrayList:
        if c == num:
            sum += 1
    return sum

    
