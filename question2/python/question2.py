import modules

number = int(input('Digite um número: ')) #Catches the number that will be analyzed
array_of_numbers = modules.read_20_num() # read 20 integers or less, if the user press -1
sum = modules.analyze_num(array_of_numbers, number) # do a sum of how many times the integer that the user did choice appears
print(f'O número {number} apareceu {sum} vezes!') # show in the prompt

'''A função "mainprogram" não depende de nenhuma variável, foi utilizada apenas para fins de organização
do código.

O programa inicia pedindo um número, para saber qual número deveremos identificar e analisar

após isso, é utilizada a função leia20num, que recebe e armazena os 20 números dados pelo usuário.

Depois, utilizamos a função analisanum, que através da função for, identifica quantas vezes
determinado número apareceu.'''