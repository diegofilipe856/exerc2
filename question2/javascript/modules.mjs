import promptSync from 'prompt-sync'

const prompt = promptSync();

export const readInt = ()=>{
    let integer = prompt("Insira um número inteiro: ")
    return integer
}

export const createArray = ()=>{
    let array = []
    for (let k = 0; k<20;k++){
        let arrayElement = prompt(`Insira o x${k}: `)
        if (arrayElement != -1){
            array.push(arrayElement);
        }
        else{
            break
        }
        
    }
    return array
}

export const fn = ()=>{
    let sum = 0;
    let integer = readInt();
    integer = Number(integer);
    const array20 = createArray();
    for (let i = 0; i<20; i++){
        if (array20[i] == integer){
            sum ++;
        }
        
    }

    console.log(`O número ${integer} apareceu ${sum} vez(es)!`)
}