import modules


list_quantity = int(input('Digite a quantidade de inteiros do array: '))

list_of_integers = modules.create_list(list_quantity)
deslocated_list = modules.deslocate_list(list_of_integers, list_quantity)
show_list = modules.print_list(deslocated_list)


'''Primeiro, importei a função "deque", da biblioteca collections. Essa função me permite
que eu rotacione o array de acordo com a vontade do usuário.

Após utilizar a função deque() e .rotate(), apenas utilizei a função "for", para percorrer todo o array
e utilizei a função end=' ', para que o resultado fosse expresso na mesma linha.
 
Professor, eu fiquei com algumas dúvidas com relação à interpretação das questões, por isso deixei
linhas de comentários, mostrando as duas formas que o programa poderia ser feito. '''