from collections import deque


def create_list(listQuantity):
    if listQuantity <= 100000:
        array = []
        for d in range(0, listQuantity):
            num = int(input(f'Digite o {d+1}º inteiro: '))
            array.append(num)
        List = deque(array)
        return List
    else:
        print('A lista não pode ter mais que 100000 inteiros')


def deslocate_list(List, listQuantity):
    qtdesloc = int(input('Qual a quantidade de deslocamentos? '))
    if qtdesloc >= 0 and qtdesloc <= listQuantity:
        List.rotate(-qtdesloc)
        return List


def print_list(List):
    for c in List:
        print(c)