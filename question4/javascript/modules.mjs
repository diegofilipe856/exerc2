import promptSync from 'prompt-sync'
const prompt = promptSync();

export const createArray = (quantityOfIntegers)=>{
    let array = []
    for (let i = 0; i<quantityOfIntegers; i++){
        let numInArray = prompt(`Insira o inteiro ${i+1}: `)
        array.push(numInArray);
    }    
    return array
}

export const rotateLeft = (array, nDeslocament)=>{
    for (let k = 0; k<nDeslocament;k++)
        array.push(array.shift())
    return array

}

