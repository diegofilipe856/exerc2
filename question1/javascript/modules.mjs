import promptSync from 'prompt-sync'


export const calculate = (mass, height)=>{
    return (mass / (height**2))

}

export const sit = (imc)=>{
    if ((imc >= 1) && (imc < 16)) { // if IMC is between 1 && 16, the mass is too lower.
        const situation = 'Muito abaixo do peso';
        return situation
    } else if ((imc >= 16) && (imc < 19)) { // if IMC is between 16 && 19, the mass is lower.
        const situation = 'Abaixo do peso'
        return situation
    }
    else if ((imc >= 19) && (imc < 25)){// if IMC is between 19 && 25, the mass is normal.
        const situation = 'Peso normal'
        return situation
}
    else if ((imc >= 25) && (imc < 30)){ // if IMC is between 25 && 30, the individual is overweight
        const situation = 'Sobrepeso'
        return situation
}
    else if ((imc >= 30) && (imc < 35)){ //if IMC is between 30 && 35, the individual is obese grade 1
        const situation = 'Obesidade grau I'
        return situation
}
    else if ((imc >= 35) && (imc < 40)) {//if IMC is between 35 && 40, the individual is obese grade 2
        const situation = 'Obesidade grau II'
        return situation
}
    else if ((imc >= 40) && (imc < 200)){ //if IMC is between 40 && 200, the individual is obese grade 3
        const situation = 'Obesidade grau III'
        return situation
}
    else{
        const situation = 'Provavelmente houve algum problema de cálculo. Tente novamente!' //Probably the user commited an error or did input a surreal number.
        return situation
}
    
}

export const fn = ()=>{
    const prompt = promptSync();

    const mass = prompt("mass: ");
    const height = prompt("height: ")
    console.log(`Massa: ${mass}, altura: ${height}`);
    let imc = calculate(mass, height)
    const situation = sit(imc)
    if ((imc >= 1) && (imc <= 200)) {
        console.log(`Seu IMC é: ${imc.toFixed(2)}`)
        console.log(`Seu status é: ${situation}`)

    }
    

}

