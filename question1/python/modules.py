from sys import modules


def calculate(height, mass): 
    return mass/(height**2) #calcula o IMC


def catch_data():
    mass = input('Digite a sua massa corporal em Kg: ').replace(',','.') # in case of the user input a brazillian number, it will be converted to float
    height = input('Digite a sua altura em metros: ').replace(',','.')
    mass = float(mass) #converts the type to float
    height = float(height)
    return mass, height


def sit(imc):
    if imc >= 1 and imc < 16: # if IMC is between 1 and 16, the mass is too lower.
        situation = 'Muito abaixo do peso'
        return situation

    elif imc >= 16 and imc < 19: # if IMC is between 16 and 19, the mass is lower.
        situation = 'Abaixo do peso'
        return situation

    elif imc >= 19 and imc < 25: # if IMC is between 19 and 25, the mass is normal.
        situation = 'Peso normal'
        return situation

    elif imc >= 25 and imc < 30: # if IMC is between 25 and 30, the individual is overweight
        situation = 'Sobrepeso'
        return situation

    elif imc >= 30 and imc < 35: #if IMC is between 30 and 35, the individual is obese grade 1
        situation = 'Obesidade grau I'
        return situation

    elif imc >= 35 and imc < 40: #if IMC is between 35 and 40, the individual is obese grade 2
        situation = 'Obesidade grau II'
        return situation

    elif imc >= 40 and imc < 200: #if IMC is between 40 and 200, the individual is obese grade 3
        situation = 'Obesidade grau III'
        return situation

    else:
        print('Provavelmente houve algum problema de cálculo. Tente novamente!') #Probably the user commited an error or did input a surreal number.
