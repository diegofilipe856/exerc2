from sys import modules


import modules


print('Olá! Bem-vindo à calculadora de IMC!')
mass, height = modules.catch_data() #Catch the mass and height of the individual, replacing comma for dot 
imc = modules.calculate(height, mass) # calcule the imc of the person and returns in the variable "imc"
situation = modules.sit(imc) # Analises the imc and gives the information of the status of health of the individual
if imc >= 1 and imc <= 200: # analises if the imc is a realistic number
    print(f'Seu IMC é: {imc:.2f}')
    print(f'Seu status é: {situation}')


'''A função "mainprogram" não depende de nenhuma variável, foi utilizada apenas para fins de organização
do código.

Logo no começo do programa, já utilizamos a primeira função criada, a "pegadados".

Nessa função, recolhemos os dados de peso e altura do usuário, utilizando também a 
função .replace(), para caso ele utilize vírgula como casa decimal.

Após, utilizamos a função calcula() para descobrir o valor do imc, e depois utilizamos a função sit,
para descobrir o status em que o usuário se encontra.

Por fim, se o número de IMC fazer sentido (estar entre valores de 1 a 200), o programa
exibirá o IMC e o status do indivíduo.

Caso o número de IMC esteja além desse limite, o programa informará que pode ter ocorrido
algum erro no momento da inserção dos dados pelo usuário.'''